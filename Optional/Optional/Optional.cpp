// Optional.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <stddef.h>
#include <iomanip>

template <typename T>
void print_obj_hex(T const & obj) {
	unsigned char const *ptr = reinterpret_cast<unsigned char const *>(&obj);
	for (size_t i = 0; i != sizeof(obj); ++i) {
		std::cout << std::hex << std::setw(2) << std::setfill('0')
			<< static_cast<unsigned int>(ptr[i]) << ' ';
	}
	std::cout << std::endl;
}

template<typename T>
class Optional {
public:
	alignas(sizeof(T)) T value_;
public:
	constexpr Optional(T value) noexcept : value_(value) {};

};

int main()
{
	Optional<int> test(2);
	std::cout << "int: " << sizeof(test) << " at " << &test << std::endl;
	print_obj_hex(test);

	Optional<const char*> test2("wow");
	std::cout << "const char*: " << sizeof(test2) << " at "<< &test2 << std::endl;
	print_obj_hex(test2);

	Optional<double> test3(2);
	std::cout << "double: " << sizeof(test3) << " at " << &test3 << std::endl;
	print_obj_hex(test3);
}
